import React, {useState } from 'react'
import { Button,Form } from 'react-bootstrap'
import { useLocation, useNavigate } from 'react-router-dom'


function SearchBox(){
	const history = useNavigate()
	const location = useLocation()

	const [keyword, setKeyword] = useState('')


	const submitHandler = (e) => {
		e.preventDefault()
		if(keyword){
			history(`/?keyword=${keyword}`)
		} else {
			history('')
		}
	}
	return(
		<Form onSubmit={submitHandler} className='d-flex'>
            <Form.Control
                type='text'
                name='q'
                onChange={(e) => setKeyword(e.target.value)}
                className='mr-sm-2 ml-sm-5'
            ></Form.Control>

            <Button
                type='submit'
                variant='outline-success'
                className='p-1'
            >
                Submit
            </Button>
        </Form>
	)
}

export default SearchBox