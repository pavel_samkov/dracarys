import React, { useState, useEffect } from 'react'
import { Carousel, Image } from 'react-bootstrap'
import { Link, useLocation, useNavigate } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'

import Loader from '../components/Loader'
import Message from '../components/Message'

import { listTopProducts } from '../actions/productActions'

function ProductCarousel(){
	const dispatch = useDispatch()

	const productTopRated = useSelector(state => state.productTopRated)
	const { loading, error, products } = productTopRated

	const history = useNavigate()
	const location = useLocation()


	useEffect(() => {
		dispatch(listTopProducts())
	}, [dispatch])

	return( loading ? <Loader />
		: error 
		? <Message variant='danger'> {error}</Message>
		:(
		<Carousel pause='hover' className='bg-dark'>
			{products.map(product=>(
				<Carousel.Item key={product.id}>
					<Link to={`/product/${product.id}`}>
						<Image src={product.image} alt={product.name} fluid/>
						<Carousel.Caption className='carousel.caption'>
							<h4>{product.name} ({product.price}PLN)</h4>
						</Carousel.Caption>
					</Link>
				</Carousel.Item>
				))}
		</Carousel>

		)
		
	)
}

export default ProductCarousel