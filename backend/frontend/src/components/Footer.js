import React from 'react'
import { Container, Row, Col } from 'react-bootstrap'


function Footer(argument) {
	return (
		<footer>
			<Container>
				<Row>
					<Col className="text-center py-3">Copyright &copy; Dracarys</Col>
				</Row>
			</Container>
		</footer>
		)
}

export default Footer