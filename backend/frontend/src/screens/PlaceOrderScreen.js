import React, { useState, useEffect } from 'react'
import { useParams, useNavigate, useLocation, Link } from 'react-router-dom'
import { Button, Row, Col, ListGroup, Image, Card} from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'

import  CheckoutSteps  from '../components/CheckoutSteps'
import  Message  from '../components/Message'

import { createOrder } from '../actions/orderActions'

import { ORDER_CREATE_RESET } from '../constants/orderConstants'


function PlaceOrderScreen(argument) {
	const history = useNavigate()
	const dispatch = useDispatch()

	const orderCreate = useSelector(state => state.orderCreate)
	const { order, error, success } = orderCreate

	const cart = useSelector(state => state.cart)

	cart.itemsPrice = cart.cartItems.reduce((acc, item) => acc + item.price * item.quantity, 0).toFixed(2)
	cart.taxPrice = Number(cart.itemsPrice * (0.17)).toFixed(2)
	cart.shippingPrice = (cart.itemsPrice > 100 ? 0 : 15.99).toFixed(2)
	cart.totalPrice = (Number(cart.itemsPrice) + Number(cart.taxPrice)  + Number(cart.shippingPrice)).toFixed(2)


	if(!cart.paymentMethod){
		history('/payment')
	}

	useEffect(() => {
		if(success){
			history(`/order/${order.id}`)
			dispatch({type:ORDER_CREATE_RESET})
		}
	},[success, history])

	const placeOrder = () => {
		dispatch(createOrder({
			orderItems: cart.cartItems,
			shippingAddress: cart.shippingAddress,
			paymentMethod: cart.paymentMethod,
			itemsPrice: cart.itemsPrice,
			shippingPrice: cart.shippingPrice,
			taxPrice: cart.taxPrice,
			totalPrice: cart.totalPrice,
		}))
	}




 	return (
		<div>
			<CheckoutSteps step1 step2 step3 step4 />
			<h1> Place Order </h1>
			<Row> 
				<Col md={8}>
					<ListGroup variant='flush'>
						<ListGroup.Item>
							<h2>Shipping</h2>

							<p>
								<strong>Shipping: </strong>
								{cart.shippingAddress.address}, {cart.shippingAddress.city}
								{'  '}
								{cart.shippingAddress.postalCode},
								{'  '}
								{cart.shippingAddress.country}
							</p>
						</ListGroup.Item>


						<ListGroup.Item>
							<h2>PaymentMethod</h2>

							<p>
								<strong>Method: </strong>
								{cart.paymentMethod}
							</p>
						</ListGroup.Item>


						<ListGroup.Item>
							<h2>Order Items</h2>
							{ cart.cartItems === 0  ? <Message variant='info'> 
								YOur cart is empty </Message> : (
								<ListGroup variant='flush'>
									{cart.cartItems.map((item, index) => (
										<ListGroup.Item key={index}>
											<Row>
												<Col md={1}>
													<Image src={item.image} alt={item.name} fluid rounded/> 
												</Col>

												<Col>
													<Link to={`/product/${item.product}`}>{item.name}</Link> 
												</Col>

												<Col md={4}>
													{item.quantity} X {item.price}PLN = {(item.quantity * item.price).toFixed(2)}PLN
												</Col>
											</Row>
										</ListGroup.Item>
										))}
								</ListGroup>
								)}	
						</ListGroup.Item>
					</ListGroup>
				</Col>


				<Col md={4}>
					<Card>
						<ListGroup variant='flush'>
							<ListGroup.Item>
								<h2>Order Summary</h2>
							</ListGroup.Item>

							<ListGroup.Item>	
								<Row>
									<Col>Item: </Col>
									<Col>{cart.itemsPrice}PLN</Col>
								</Row>
							</ListGroup.Item>

							<ListGroup.Item>	
								<Row>
									<Col>Shipping: </Col>
									<Col>{cart.shippingPrice}PLN</Col>
								</Row>
							</ListGroup.Item>

							<ListGroup.Item>	
								<Row>
									<Col>Tax: </Col>
									<Col>{cart.taxPrice}PLN</Col>
								</Row>
							</ListGroup.Item>

							<ListGroup.Item>	
								<Row>
									<Col>Total: </Col>
									<Col>{cart.totalPrice}PLN</Col>
								</Row>
							</ListGroup.Item>

							<ListGroup.Item>
								{error && <Message variant='danger'> {error}</Message> }
							</ListGroup.Item>

							<ListGroup.Item>
								<Button 
									type='button' 
									className='btn-block'
									disabled={cart.cartItems === 0}
									onClick={placeOrder}
								> 
										Place Order 

								</Button>
							</ListGroup.Item>	

						</ListGroup>
					</Card>
				</Col>
			</Row>


			
		</div>
		)
}

export default PlaceOrderScreen