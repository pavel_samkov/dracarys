import React, { useState, useEffect } from 'react'
import { Row, Col } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate, useLocation } from 'react-router-dom'


import Product from '../components/Product'
import Loader from '../components/Loader'
import Message from '../components/Message'
import Paginate from '../components/Paginate'
import ProductCarousel from '../components/ProductCarousel'

import { listProducts } from '../actions/productActions'


function HomeScreen(argument) {
	const dispatch = useDispatch()
	const history = useNavigate()
	const location = useLocation()

	const productList = useSelector(state => state.productList)
	const { error, loading, products, pages, page } = productList

	let keyword = location.search
	console.log(keyword)
	useEffect(() => {
		dispatch(listProducts(keyword))
	}, [dispatch, keyword])

 	return (
		<div>
			{!keyword && <ProductCarousel />}
			
			<h1>Latest Products</h1>
			{loading ? <Loader /> 
				: error ? <Message variant="danger"> { error }</Message>
				: 
				<div>
					<Row>
						{products.map(product => (
							<Col key={product.id} sm={12} md={6} lg={4} xl={3}>
								<Product product={product} />
							</Col>
							))}
					</Row>
					<Paginate page={page} pages={pages} keyword={keyword} />
					
				</div>
			}
		</div>
		)
}

export default HomeScreen