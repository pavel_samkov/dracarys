from django.urls import path
from ecommerce.views import order_views as views



urlpatterns = [
   path('add/', views.addOrderItems, name='orders_add'),
   path('myorders/', views.getMyOrders, name='my_orders'),
   path('', views.getOrders, name='orders'),

   path('<int:pk>/deliver/', views.updateOrderToDelivered, name='update_order_to_delivered'),
   path('<int:pk>/', views.getOrderById, name='get_order_by_id'),
   path('<int:pk>/pay/', views.updateOrderToPaid, name='update_order_to_paid'),

]