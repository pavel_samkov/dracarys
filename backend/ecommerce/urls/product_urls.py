from django.urls import path
from ecommerce.views import product_views as views


urlpatterns = [
	path('', views.getProducts, name='products'), 
	path('create/', views.createProduct, name='product_create'),
	path('upload/', views.uploadImage, name='image_upload'),
	path('top/', views.getTopProducts, name='top_products'),

	path('<int:pk>/', views.getProduct, name='product'), 
	path('<int:pk>/reviews/', views.createProductReview, name='create_product_review'), 

	path('update/<int:pk>/', views.updateProduct, name='product_update'), 
	path('delete/<int:pk>/', views.deleteProduct, name='product_delete'), 
]