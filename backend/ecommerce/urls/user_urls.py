from django.urls import path
from ecommerce.views import user_views as views



urlpatterns = [
   

	path('routes/', views.getRoutes, name='routes'), 

	path('', views.getUsers, name='users'), 
	path('login/', views.MyTokenObtainPairView.as_view(), name='token_obtain_pair'),
	path('register/', views.registerUser, name='register'),
	path('profile/', views.getUserProfile, name='user'), 
	path('profile/update/', views.updateUserProfile, name='update_user_profile'),

	path('<int:pk>/', views.getUserById, name='get_user_by_id'),

	path('delete/<int:pk>/', views.deleteUser, name='delete_user'),

	path('update/<int:pk>/', views.updateUser, name='update_user'),
]