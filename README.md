# About 
E-commerce shisha-shop. Shisha it is great and ancient culture and if you are a part of this culture this page is for you. Here you can find all what you need for a pleasant time with your friends. Hookahs, bowls, tobacco and many many more. User friendly interface, pay with cards, Paypal, or even crypto. Delivered in any place on the planet. Dive into this culture with Dracarys.

# SQL Schema
[https://drawsql.app/me--2/diagrams/dracarys](https://drawsql.app/me--2/diagrams/dracarys)

# Dracarys
[E-commerce shisha shop](https://dracarysshop.herokuapp.com)

# Download & Setup Instructions

* 1 - Clone project: git clone https://github.com/pavel_samkov/dracarys/
* 2 - cd dracarys
* 3 - Create virtual environment: virtualenv myenv
* 4 - myenv\scripts\activate
* 5 - pip install -r requirements.txt
* 6 - python manage.py runserver

# Install react modules
* 1 - cd frontend
* 2 - npm install
